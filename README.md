# Introduction
Coherence*Web is a great feature which allows you to store your HTTP sessions in a Coherence Cluster, as well as the ORM cached objects. Doing this allows your to de-couple the HTTP sessions from your JVM’s that are running your web apps, (which can free up JVM memory) and take advantage of the RASP (Reliability, Availability, Scalability and Performance) capabilities of Coherence for storage.

With this separation you can also more easily scale and manage and your HTTP session tier as well as utilise the wide array of session management options that Coherence*Web provides.

This how-to will explain the configuration of Coherence*Web in WebLogic Server 12c and deploy a basic web application that utilises this.

### Setup

Let's assume that you already have installed WebLogic Server 12 and created a domain with node manager running (and a machine defined).

We are going to create two WebLogic Server Clusters and a Coherence cluster into which we shall place both WLS clusters. This will ensure that the managed servers in both WLS clusters are associated with a common set of Coherence caching services.

- StorageTier: 2 Coherence managed servers storage1 and storage2. This tier will hold the HTTP session data.
- ClientTier: 2 regular application managed servers client1 and client2 – We will deploy our application to this tier.

> Typically you would have multiple servers in each tier and have a load balancer in front, but for this example, we will just 2 which is the minimus to demonstrate session fail-over.

Once you have your AdminServer and node manager up and running, login to the console to carry out the following steps.

##### Create a Coherence Cluster

- Navigate to Domain Structure, and then Environment and click on Coherence Clusters.
- Give it a name and leave the defaults.  Do not target it the AdminServer as we will target it in a moment.
Coherence Cluster

![N|Solid](https://coherencedownunder.files.wordpress.com/2014/05/1_coherence_cluster.png)

##### Create the WebLogic Clusters, place both your WLS clusters in the Coherence cluster and configure Coherence storage for each WLS cluster

- Navigate to the Clusters link and create 2 clusters. StorageTier and ClientTier.
- Leave the defaults as is.
- Select the StorageTier cluster and click on the Coherence tab.
- Select your newly created Coherence cluster and click Save.
- Select Coherence Web Local Storage Enabled to ensure any managed servers in this cluster store Coherence*Web session data.
- Click Save to continue.

![N|Solid](https://coherencedownunder.files.wordpress.com/2014/05/2_storage_tier.png?w=640&h=255)

- Select the ClientTier cluster and click on the Coherence tab.
- Select your newly created Coherence cluster and click Save.
- De-select Local Storage Enabled and click Save. We don’t want to actually store HTTP sessions in the client tier, but we want to be able to access them.

![N|Solid](https://coherencedownunder.files.wordpress.com/2014/05/3_client_tier.png?w=640&h=257)

##### Create Managed Servers to populate each WLS cluster.

Note the managed servers added to each cluster will inherit their Coherence config form the cluster wide values which you set above. This means the Coherence managed servers added to the Storage Tier WLS cluster will be providing session storage while the Regular managed servers in the Client Tier WLS cluster will be coherence clients.

- Navigate to the Servers link
- Create a new managed server called client1 and assign it to the ClientTier as well as the machine you created. Set the listener port to 7005.
- Create a new managed server called storage1 and assign it to the StorageTier as well as the machine you created. Set the listener port to 7007.
- Clone storage1 and name it storage2 and change the listener port to 7009.
- Start up you managed servers.

![N|Solid](https://coherencedownunder.files.wordpress.com/2014/05/4_servers.png)

### Deployment

Now we need to Deploy applications which will demonstrate session fail-over and management features we have configured

- ClusterJSP: Shows session failover.

#### Deployment of ClusterJSP
Deploy ClusterJSP as a directory deployment rather than a archive deployment. This will make modifications of the deployment descriptors easier.  You can download the file required from here.

###### Deploy the Application

Deploy the application as you usually do to publish a war file:

- In the Admin. Console navigate to the Deployments page.
- Click the Install button and navigate to the applications directory where you should find the clusterjsp-ear directory.
- Click the select button to the left of the folder and click the Next button at the bottom of the page.
- Leave Install as application selected and click Next.
- Select ClientTier cluster and click Next.
- Leave the defaults and click Finish.

###### Test the application

- In a browser access the clusterjsp with the following url: http://<ip>:<port>/clusterjsp
- Follow the instructions at the bottom of the displayed page.
- To access the page from the other server in the client tier cluster change the the port number in the browser and hit enter. Notice the session attribute and value which you had added are not present because we have not yet enabled Coherence*Web.

![N|Solid](https://coherencedownunder.files.wordpress.com/2014/05/screen-shot-2014-05-27-at-2-30-50-pm.png)

So far, the application has to be working as any other application, storing the session data in the JVM memory where the application server is running.

###### Enable Coherence*Web Session Storage

- Create a WLS deployment descriptor to enable Coherence*Web sessions by saving the text below into a new file called WEB-INF/weblogic.xml and add it to the war file.

```sh
<xml version="1.0"?>
 
<weblogic-web-app xmlns="http://www.bea.com/ns/weblogic/weblogic-web-app"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.bea.com/ns/weblogic/weblogic-web-app http://www.bea.com/ns/weblogic/weblogic-web-app/1.0/weblogic-web-app.xsd">
 
<session-descriptor>
<persistent-store-type>coherence-web</persistent-store-type>
</session-descriptor>
 
</weblogic-web-app>
```
- In the Admin. Console navigate to the Deployments page.
- Update the deployment by click the Select button next to the clusterjsp application and click the Update button.
- Repeat your previous test. This time you should see your Session attributes are present regardless of which client tier server you access.
- Stop the servers one at a time in each tier and notice when the session data is no longer available.

> Note
> For a multi-machine WebLogic  app-tier cluster, to ensure all machines have a common cookie path for the app, you must access the cluster jsp through a load balancer.

It’s very easy to scale out your HTTP session tier, just add another managed server to your StorageTier.

## Use the JVisualVM Plug-in from the Coherence Incubator Project to monitor the Coherence*Web session data

I’m using the Coherence JVisualVM Plug-in from the Coherence Incubator for viewing MBean information for the rest of this demo. You could just as well browse the MBean tree, but the Plug-in shows this in a nicer format.

###### Install the Coherence VisualVM plugin brief steps

- Download the latest pre-built plug-in module (NBM) from search.maven.org and save it to a local directory.
- Start JVisualVM by running jvisualvm or jvisualvm.exe (for Windows) from the bin directory under your Java home.
- Choose Tools -> Plugins.
- Click on the ‘Downloads’ tab.
- Click ‘Add Plugin’ and choose the file coherence-jvisualvm-12.2.0.nbm (from the location where you saved above).
- Click ‘Install’ and follow the prompts to install.

###### Once installed, connect to either a remote or local JMX enabled Coherence cache server process using JMX

Restart jvisualvm with the following command (I have used UNIX style backslash for line continuation. If you are on Windows then please ensure the command is on one line.)

```sh
$JAVA_HOME/bin/jvisualvm --cp:a \
$WL_HOME/server/lib/wljmxclient.jar:$WL_HOME/server/lib/weblogic.jar \
-J-Djmx.remote.protocol.provider.pkgs=weblogic.management.remote \
-J-Dcom.oracle.coherence.jvisualvm.disable.mbean.check=true
```

- To create a connection right-click on either ‘Local’ or ‘Remote’ under the ‘Applications’ tree. Use the following for the connection URI and enter the credentials for the WebLogic Server Admin: (the following should contain no spaces)
service:jmx:iiop://hostname:port/jndi/weblogic.management.mbeanservers.domainruntime
- Once you have opened the newly created connection you will see the ‘Coherence Tab’.

After connecting to the domain runtime mbean server, you should see the ‘Coherence’ tab displayed, select this tab and then the ‘Coherence*Web’ tab and you will see that there is a session that has been created.

![N|Solid](https://coherencedownunder.files.wordpress.com/2014/05/5_coherence_web.png)

If you look at the ‘Caches’ tab, you will see the size of the session-storage and session-overflow caches are updated.

![N|Solid](https://coherencedownunder.files.wordpress.com/2014/05/6_caches.png?w=640&h=296)

You can see from the screenshot below, that you are able to view the number of sessions created and any session-reaping, which is cleaning up of old sessions.

![N|Solid](https://coherencedownunder.files.wordpress.com/2014/05/7_more_sessions.png?w=640&h=323)